-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: localhost    Database: exodia
-- ------------------------------------------------------
-- Server version	8.0.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `card`
--

DROP TABLE IF EXISTS `card`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `card` (
  `id_card` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `imgUrl` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `energy` int(11) DEFAULT NULL,
  `hp` int(11) DEFAULT NULL,
  `defence` int(11) DEFAULT NULL,
  `attack` int(11) DEFAULT NULL,
  `prize` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_card`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `card`
--

LOCK TABLES `card` WRITE;
/*!40000 ALTER TABLE `card` DISABLE KEYS */;
INSERT INTO `card` VALUES (1,'dragonBlanc','https://ygoprodeck.com/pics/89631139.jpg',10,10,10,10,10);
/*!40000 ALTER TABLE `card` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `card_reference`
--

DROP TABLE IF EXISTS `card_reference`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `card_reference` (
  `dtype` varchar(31) CHARACTER SET latin1 NOT NULL,
  `id` int(11) NOT NULL,
  `affinity` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `description` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `family` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `img_url` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `name` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `small_img_url` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `attack` float DEFAULT NULL,
  `defence` float DEFAULT NULL,
  `energy` float DEFAULT NULL,
  `hp` float DEFAULT NULL,
  `price` float DEFAULT NULL,
  `store_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK4yudh68smbrmwnoyvpcjpyxh` (`store_id`),
  KEY `FKoohvupj0o1g5mnfnrra1jvb6a` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `card_reference`
--

LOCK TABLES `card_reference` WRITE;
/*!40000 ALTER TABLE `card_reference` DISABLE KEYS */;
INSERT INTO `card_reference` VALUES ('CardReference',1,'affinity0','description0','family0','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name0','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',2,'affinity2','description2','family2','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name2','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',3,'affinity4','description4','family4','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name4','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',4,'affinity6','description6','family6','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name6','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',5,'affinity8','description8','family8','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name8','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardModel',7,'Lumiere','Yeux Bleus','Dragon','http://www.otk-expert.fr/cartes/yugioh_ext/SS02/SS02-1.jpg','DragonBlanc','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',36,56,100,38,400,NULL,14),('CardModel',8,'Tenebres','Il est sombre','Magicien','https://www.otk-expert.fr/cartes/yugioh_ext/DUPO/DUPO-101.jpg','Magicien des Tenebres','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',2,54,100,41,700,NULL,6),('CardModel',9,'Lumiere','3 Dragons Blancs','Dragon','http://www.otk-expert.fr/cartes/yugioh_ext/SBLS/SBLS-12.jpg','UltimeDragonBlanc','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',58,73,100,66,1200,NULL,14),('CardModel',11,'Tenebres','Boule de Poil','Demon','https://www.otk-expert.fr/cartes/yugioh_ext/AC19/AC19-1.jpg','Kuriboh','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',26,43,100,41,200,NULL,6),('CardModel',15,'Piege','Broie les Cartes','Normal','http://www.otk-expert.fr/cartes/yugioh_ext/SR06/SR06-31.jpg','Virus','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',10,12,100,86,800,NULL,NULL),('CardModel',16,'Magie','Ca retrecit','Rapide','http://www.otk-expert.fr/cartes/yugioh_ext/LCKC/LCKC-43.jpg','Retrecissement','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',52,31,100,73,1000,NULL,14),('CardReference',17,'affinity0','description0','family0','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name0','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',18,'affinity2','description2','family2','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name2','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',19,'affinity4','description4','family4','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name4','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',20,'affinity6','description6','family6','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name6','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',21,'affinity8','description8','family8','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name8','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',22,'affinity0','description0','family0','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name0','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',23,'affinity2','description2','family2','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name2','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',24,'affinity4','description4','family4','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name4','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',25,'affinity6','description6','family6','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name6','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',26,'affinity8','description8','family8','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name8','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',27,'affinity0','description0','family0','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name0','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',28,'affinity2','description2','family2','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name2','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',29,'affinity4','description4','family4','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name4','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',30,'affinity6','description6','family6','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name6','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',31,'affinity8','description8','family8','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name8','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',32,'affinity0','description0','family0','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name0','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',33,'affinity2','description2','family2','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name2','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',34,'affinity4','description4','family4','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name4','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',35,'affinity6','description6','family6','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name6','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',36,'affinity8','description8','family8','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name8','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',37,'affinity0','description0','family0','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name0','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',38,'affinity2','description2','family2','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name2','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',39,'affinity4','description4','family4','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name4','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',40,'affinity6','description6','family6','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name6','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',41,'affinity8','description8','family8','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name8','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',42,'affinity0','description0','family0','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name0','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',43,'affinity2','description2','family2','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name2','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',44,'affinity4','description4','family4','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name4','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',45,'affinity6','description6','family6','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name6','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',46,'affinity8','description8','family8','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name8','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',47,'affinity0','description0','family0','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name0','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',48,'affinity2','description2','family2','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name2','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',49,'affinity4','description4','family4','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name4','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',50,'affinity6','description6','family6','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name6','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',51,'affinity8','description8','family8','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name8','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',52,'affinity0','description0','family0','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name0','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',53,'affinity2','description2','family2','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name2','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',54,'affinity4','description4','family4','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name4','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',55,'affinity6','description6','family6','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name6','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',56,'affinity8','description8','family8','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name8','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',58,'affinity0','description0','family0','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name0','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',59,'affinity2','description2','family2','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name2','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',60,'affinity4','description4','family4','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name4','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',61,'affinity6','description6','family6','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name6','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',62,'affinity8','description8','family8','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name8','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',63,'affinity0','description0','family0','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name0','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',64,'affinity2','description2','family2','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name2','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',65,'affinity4','description4','family4','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name4','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',66,'affinity6','description6','family6','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name6','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',67,'affinity8','description8','family8','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name8','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',68,'affinity0','description0','family0','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name0','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',69,'affinity2','description2','family2','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name2','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',70,'affinity4','description4','family4','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name4','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',71,'affinity6','description6','family6','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name6','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',72,'affinity8','description8','family8','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name8','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',73,'affinity0','description0','family0','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name0','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',74,'affinity2','description2','family2','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name2','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',75,'affinity4','description4','family4','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name4','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',76,'affinity6','description6','family6','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name6','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',77,'affinity8','description8','family8','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name8','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',78,'affinity0','description0','family0','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name0','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',79,'affinity2','description2','family2','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name2','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',80,'affinity4','description4','family4','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name4','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',81,'affinity6','description6','family6','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name6','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',82,'affinity8','description8','family8','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name8','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',83,'affinity0','description0','family0','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name0','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',84,'affinity2','description2','family2','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name2','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',85,'affinity4','description4','family4','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name4','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',86,'affinity6','description6','family6','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name6','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',87,'affinity8','description8','family8','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name8','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',88,'affinity0','description0','family0','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name0','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',89,'affinity2','description2','family2','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name2','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',90,'affinity4','description4','family4','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name4','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',91,'affinity6','description6','family6','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name6','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',92,'affinity8','description8','family8','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name8','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',93,'affinity0','description0','family0','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name0','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',94,'affinity2','description2','family2','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name2','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',95,'affinity4','description4','family4','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name4','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',96,'affinity6','description6','family6','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name6','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',97,'affinity8','description8','family8','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name8','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',98,'affinity0','description0','family0','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name0','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',99,'affinity2','description2','family2','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name2','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',100,'affinity4','description4','family4','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name4','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',101,'affinity6','description6','family6','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name6','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',102,'affinity8','description8','family8','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name8','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',103,'affinity0','description0','family0','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name0','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',104,'affinity2','description2','family2','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name2','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',105,'affinity4','description4','family4','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name4','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',106,'affinity6','description6','family6','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name6','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',107,'affinity8','description8','family8','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name8','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardModel',109,'Tenebres','Yeux rouges','Dragon','http://www.otk-expert.fr/cartes/yugioh_ext/SS02/SS02-21.jpg','Dragon Noir','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',5,16,100,33,111,NULL,108),('CardModel',110,'Vent','Gardien de la Forteresse','Dragon','http://www.otk-expert.fr/cartes/yugioh_ext/YGLD/YGLD-10.jpg','Dragon Aile','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',79,88,100,42,111,NULL,NULL),('CardModel',111,'Lumiere','Son heure tourne','Magicien','http://www.otk-expert.fr/cartes/yugioh_ext/SS02/SS02-27.jpg','Magicien du Temps','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',19,36,100,73,111,NULL,108),('CardModel',112,'Lumiere','Fantastique defense','Magicien','http://www.otk-expert.fr/cartes/yugioh_ext/SBAD/SBAD-3.jpg','Elphe Mystique','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',1,43,100,26,111,NULL,NULL),('CardModel',113,'Lumiere','Il est tres fort','Dieu Egypsien','http://www.otk-expert.fr/cartes/yugioh_ext/TN19/TN19-7.jpg','Obelisk le Tourmenteur','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',80,89,100,80,111,NULL,NULL),('CardReference',114,'affinity0','description0','family0','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name0','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',115,'affinity2','description2','family2','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name2','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',116,'affinity4','description4','family4','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name4','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',117,'affinity6','description6','family6','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name6','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',118,'affinity8','description8','family8','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name8','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',119,'affinity0','description0','family0','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name0','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',120,'affinity2','description2','family2','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name2','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',121,'affinity4','description4','family4','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name4','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',122,'affinity6','description6','family6','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name6','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',123,'affinity8','description8','family8','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name8','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',124,'affinity0','description0','family0','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name0','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',125,'affinity2','description2','family2','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name2','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',126,'affinity4','description4','family4','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name4','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',127,'affinity6','description6','family6','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name6','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',128,'affinity8','description8','family8','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name8','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',129,'affinity0','description0','family0','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name0','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',130,'affinity2','description2','family2','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name2','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',131,'affinity4','description4','family4','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name4','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',132,'affinity6','description6','family6','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name6','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',133,'affinity8','description8','family8','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name8','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',134,'affinity0','description0','family0','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name0','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',135,'affinity2','description2','family2','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name2','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',136,'affinity4','description4','family4','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name4','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',137,'affinity6','description6','family6','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name6','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',138,'affinity8','description8','family8','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name8','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',139,'affinity0','description0','family0','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name0','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',140,'affinity2','description2','family2','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name2','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',141,'affinity4','description4','family4','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name4','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',142,'affinity6','description6','family6','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name6','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',143,'affinity8','description8','family8','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name8','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',144,'affinity0','description0','family0','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name0','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',145,'affinity2','description2','family2','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name2','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',146,'affinity4','description4','family4','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name4','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',147,'affinity6','description6','family6','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name6','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CardReference',148,'affinity8','description8','family8','http://medias.3dvf.com/news/sitegrab/gits2045.jpg','name8','https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `card_reference` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hibernate_sequence`
--

DROP TABLE IF EXISTS `hibernate_sequence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hibernate_sequence`
--

LOCK TABLES `hibernate_sequence` WRITE;
/*!40000 ALTER TABLE `hibernate_sequence` DISABLE KEYS */;
INSERT INTO `hibernate_sequence` VALUES (149),(149),(149);
/*!40000 ALTER TABLE `hibernate_sequence` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `message`
--

DROP TABLE IF EXISTS `message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `message` (
  `id` int(11) NOT NULL,
  `channelid` varchar(255) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `text` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKmbyfvc1j051dh2q4vscxghdee` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `message`
--

LOCK TABLES `message` WRITE;
/*!40000 ALTER TABLE `message` DISABLE KEYS */;
/*!40000 ALTER TABLE `message` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `store_model`
--

DROP TABLE IF EXISTS `store_model`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `store_model` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `store_model`
--

LOCK TABLES `store_model` WRITE;
/*!40000 ALTER TABLE `store_model` DISABLE KEYS */;
/*!40000 ALTER TABLE `store_model` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_model`
--

DROP TABLE IF EXISTS `user_model`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_model` (
  `id` int(11) NOT NULL,
  `account` float NOT NULL,
  `email` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `last_name` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `login` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `pwd` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `sur_name` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_model`
--

LOCK TABLES `user_model` WRITE;
/*!40000 ALTER TABLE `user_model` DISABLE KEYS */;
INSERT INTO `user_model` VALUES (6,600,'yugi@cpe.fr','Muto','yugi','muto','Yugi'),(14,1000,'seto@cpe.fr','Kaiba','seto','kaiba','Seto'),(108,400,'joey@cpe.fr','Wheeler','joey','wheeler','Joey');
/*!40000 ALTER TABLE `user_model` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `pwd` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `account` float DEFAULT NULL,
  `lastName` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `surName` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `email` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-10-26 13:02:57
