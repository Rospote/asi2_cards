**Card Game Manager**

Card Game Manager is a 1 month long project that we developped as a group of 4 in 5th year of engineering school. The goal of the project was to introduce us to ReactJS and NodeJS through a complex project. 


**Features**

With the application, you can:

* Create/Delete an account and log in/out. Each new user gets new cards selected randomly. 
*  Sell and buy card through a store.
*  Duel someone thanks to our matchmaking system
*  Chat in real time with your opponent

![alt text](yugioh-manager/public/logo512.png "Title Text")


**Technologies**

We're using the Framework ReactJS for all the frontend. We were given a Spring Boot backend by the teacher, we then added a NodeJS backend for the chat. The two backends are linked via an ActiveMQ message broker to store the logs of the chat in the MYSQL database.

**Credits**

Special thanks to my coworkers Pheng ZU, Samuel BERGEROT, Thomas LEMOINE and my teacher Jacques SARYDARYAN for all the help provided.