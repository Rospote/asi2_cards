const ScreenLabel = {
  CONNEXION: "/connexion",
  LANDING: "/",
  BUY: "/buy",
  SELL: "/sell",
  MATCH_MAKING: "/game",
  GAME: "/game/:id",
  SIGN_UP:"/signup",
  PROFILE:"/profile"
};

export default ScreenLabel;
