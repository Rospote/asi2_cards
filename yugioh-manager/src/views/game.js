import React from "react";
import Container from "@material-ui/core/Container";
import Header from "../components/header";
import Grid from "@material-ui/core/Grid";
import PlayGround from "../components/playGround/playGround";

import Chat from "../components/chat/chat";

const Game = () => {
  return (
    <Grid
      container
      spacing={1}
      wrap="nowrap"
      alignItems="stretch"
      style={{ padding: "20px" }}
    >
      <PlayGround />
      <Grid
        item
        xs={2}
        direction="column"
        justify="center"
        alignItems="space-between"
      >
        <Chat />
      </Grid>
    </Grid>
  );
};

export default Game;
