import React, { useState, useEffect } from "react";

import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";

import SimpleArray from "../components/array.js";
import { connect } from "react-redux";

import Button from "../components/card/button";
import Card from "../components/card/card";

import { removeCardToStore, selectCard, loadCardToStore } from "../ducks/currentStoreCards";
import { buyCard } from "../ducks/currentUserCards";
import { deleteMoney } from "../ducks/currentUser";
import { buyCardRequest, getcardsInStore } from "../api/springBackend/card";


const useStyles = makeStyles(theme => ({
  container: {
    marginTop: theme.spacing(3)
  },
  card: {
    justifyContent: "center"
  }
}));

const Buy = ({
  cardStore,
  removeCard,
  buyCard,
  deleteMoney,
  selectCard,
  user,
  loadCardToStore
}) => {
  const classes = useStyles();
  const [selectedLine, setSelectedLine] = useState(null);
  const [selectedCard, setSelectedCard] = useState(cardStore.currentCard);


  useEffect(() => {
    getcardsInStore().then(response => {
    loadCardToStore(response);
    });
  },[]);

  const handleSelectItem = card => {
    setSelectedLine(card.id);
    setSelectedCard(card);
    selectCard(card);
    console.log(selectedCard);
  };

  const handleBuyButton = () => {
    console.log(selectedCard);
    console.log(user.wallet)
    if (user.wallet > selectedCard.price) {
      buyCardRequest(selectedCard, user.user.id)
      removeCard(selectedCard.id);
      buyCard(selectedCard);
      setSelectedCard({});
      deleteMoney(selectedCard.price);
    }
  };

  const _renderCard = () => {
    if (
      selectedCard !== null &&
      selectedCard !== undefined &&
      Object.keys(selectedCard).length !== 0 &&
      selectedCard.constructor === Object
    ) {
      return (
        <div className={classes.card}>
          <Card type="picture" card={selectedCard} size="large" />
          <Button
            label={`Buy (${selectedCard.price} $)`}
            onClick={handleBuyButton}
          />
        </div>
      );
    }
  };

  return (
    <Container maxWidth="xl" wrap="nowrap" alignItems="stretch">
      <Grid container spacing={3}>
        <Grid className={classes.container} item xs>
          <SimpleArray
            cardList={cardStore.storeCardList}
            selectedLine={selectedLine}
            handleClickLine={handleSelectItem}
          />
        </Grid>
        <Grid item>{_renderCard()}</Grid>
      </Grid>
    </Container>
  );
};

const mapDispatchToProps = dispatch => {
  return {
    selectCard: card => dispatch(selectCard(card)),
    removeCard: id => dispatch(removeCardToStore(id)),
    buyCard: card => dispatch(buyCard(card)),
    deleteMoney: money => dispatch(deleteMoney(money)),
    loadCardToStore: cards =>dispatch(loadCardToStore(cards))
  };
};

const mapStateToProps = state => ({
  cardStore: state.cardStore,
  user: state.user
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Buy);
