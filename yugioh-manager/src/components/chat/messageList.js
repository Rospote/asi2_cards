import React, { useEffect } from "react";
import Message from "./message";

const MessageList = ({ messageList, name }) => {
  return (
    <div>
      {messageList.map((message, index) => (
        <Message
          key={index}
          user={message.user}
          name={name}
          message={message.message}
        />
      ))}
    </div>
  );
};

export default MessageList;
