import React from "react";
import TextField from "@material-ui/core/TextField";
import { height } from "@material-ui/system";

const SendMessageForm = ({ onSubmit, setMessage, message }) => {
  return (
    <form onSubmit={onSubmit}>
      <TextField
        id="outlined-full-width"
        placeholder="Placeholder"
        fullWidth
        margin="normal"
        variant="outlined"
        onChange={({ target: { value } }) => setMessage(value)}
        value={message}
        InputLabelProps={{
          shrink: true
        }}
      />
    </form>
  );
};

export default SendMessageForm;
