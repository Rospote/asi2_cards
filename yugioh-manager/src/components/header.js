import React from "react";

import { Link } from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import User from "./user/user";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import Menu from "@material-ui/core/Menu";
import { connect } from "react-redux";

import ScreenLabel from "../constants/screenLabel"

const useStyles = makeStyles(theme => ({
  grow: {
    flexGrow: 1
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    })
  },
  balance: {
    marginLeft: "3%"
  },
  user: {
    marginLeft: "90%"
  },
  container: {
    justifyContent: "space-between"
  },
  menuButton: {
    marginRight: 36
  },
  hide: {
    display: "none"
  }
}));

function Header({ onToogleSidebar, user }) {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleMenu = event => {
    setAnchorEl(event.currentTarget);
  };

  const handleDisconect = () => {
    console.log("Disconnect user");
  };
  const handleMenuClose = () => {
    setAnchorEl(null);
  };

  const isMenuOpen = Boolean(anchorEl);

  const _renderMenu = (
    <Menu
      anchorEl={anchorEl}
      anchorOrigin={{ vertical: "top", horizontal: "right" }}
      keepMounted
      transformOrigin={{ vertical: "top", horizontal: "right" }}
      open={isMenuOpen}
      onClose={handleMenuClose}
    >
      <MenuItem component={Link} to={ScreenLabel.CONNEXION} onClick={handleDisconect}>
        Log out
      </MenuItem>
      <MenuItem component={Link} to={ScreenLabel.PROFILE} onClick={handleMenuClose}>Profile</MenuItem>
      <MenuItem onClick={handleMenuClose}>My account</MenuItem>
    </Menu>
  );

  return (
    <div>
      <AppBar className={classes.appBar} position="fixed">
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={onToogleSidebar}
            edge="start"
            className={classes.menuButton}
          >
            <MenuIcon />
          </IconButton>
          <User balance={user.wallet} type="balance" />
          <div className={classes.grow} />
          <User
            name={`${user.user.surName}  ${user.user.lastName}`}
            type="avatar"
            onClick={handleMenu}
          />
          {_renderMenu}
        </Toolbar>
      </AppBar>
    </div>
  );
}

const mapStateToProps = state => ({
  user: state.user
});

export default connect(mapStateToProps)(Header);
