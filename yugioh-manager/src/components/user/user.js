import React, { Component } from "react";

import Avatar from "../user/avatar";
import Balance from "../user/balance";

function User({ name, balance, type, onClick, size, enabledText }) {
  const displayUser = () => {
    if (type === "avatar") {
      return (
        <Avatar
          name={name}
          onClick={onClick}
          size={size}
          enabledText={enabledText}
        />
      );
    }
    if (type === "balance") {
      return <Balance value={balance} />;
    }
  };

  return <>{displayUser()}</>;
}

export default User;
