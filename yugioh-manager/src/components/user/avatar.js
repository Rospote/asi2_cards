import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Avatar from "@material-ui/core/Avatar";
import { deepOrange } from "@material-ui/core/colors";

const useStyles = makeStyles({
  avatar: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center"
  },
  bigAvatar: {
    color: "#fff",
    backgroundColor: deepOrange[500]
  },
  smallAvatar: {
    color: "#fff",
    backgroundColor: deepOrange[500],
    width: 30,
    height: 30
  },
  tinyAvatar: {
    color: "#fff",
    backgroundColor: deepOrange[500],
    width: 15,
    height: 15
  }
});

export default function UserAvatar({
  name,
  imgUrl,
  onClick,
  size,
  enabledText
}) {
  const classes = useStyles();
  const surname = name.slice(0, 2);

  const _renderText = () => {
    if (enabledText) return name;
  };

  const _renderAvatar = () => {
    switch (size) {
      case "tiny":
        return (
          <Avatar className={classes.tinyAvatar}>
            <p style={{ fontSize: 10 }}>{surname}</p>
          </Avatar>
        );
      case "small":
        return <Avatar className={classes.smallAvatar}>{surname}</Avatar>;
      case "big":
        return <Avatar className={classes.bigAvatar}>{surname}</Avatar>;
      default:
        return <Avatar className={classes.bigAvatar}>{surname}</Avatar>;
    }
  };

  return (
    <div
      className={classes.avatar}
      onClick={onClick}
      justify="center"
      alignItems="center"
    >
      {_renderAvatar()}
      {_renderText()}
    </div>
  );
}
