import React from "react";

import {Link} from "react-router-dom"
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";

const SidebarItem = ({ text, icon, url }) => {
  return (
    <ListItem button component={Link} to={"/sell"} >
      <ListItemIcon>{icon}</ListItemIcon>
      <ListItemText primary={text} />
    </ListItem>
  );
};

export default SidebarItem;
