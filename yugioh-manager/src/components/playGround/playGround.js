import React, {useContext} from "react";
import { connect } from "react-redux";

import { makeStyles } from "@material-ui/core/styles";
import Divider from "@material-ui/core/Divider";

import SocketContext from "../../container/socketContext";

import {
  selectOpponentCard,
  selectUserCard
} from "../../ducks/currentGameCards";

import LeftPart from "./leftPart";
import MiddlePart from "./middlePart";
import RightPart from "./rightPart";

const useStyles = makeStyles({
  container: {
    backgroundColor: "red"
  }
});

function PlayGround({
  selectOpponentCard,
  selectUserCard,
  opponentCards,
  userCards,
  currentOpponentCard,
  currentUserCard,
  userName,
  opponentName,
  palyground,
  gameId
}) {
  const classes = useStyles();
  const socket = useContext(SocketContext);

  const handleAttack = () => {
    // attack player

    //emit socket 

    const turn ={id: gameId}
    socket.emit("play attack turn", turn);
    console.log("attack");
  };

  const handleEndTurn = () => {
    // switch user turn
    const turn ={id: gameId}
    //emit socket
    socket.emit("end turn", turn);
    console.log("end turn");
  };

  const handleSelectUCard = card => {
    selectUserCard(card);
  };

  const handleSelectOCard = card => {
    selectOpponentCard(card);
  };

  return (
    <>
      <LeftPart userName={opponentName} opponentName={userName} onClickEndTurn={handleEndTurn} />
      <Divider orientation="vertical" />
      {console.log(palyground)}
      <MiddlePart
        opponentCards={opponentCards}
        userCards={userCards}
        onSelectUCard={handleSelectUCard}
        onSelectOCard={handleSelectOCard}
      />
      <Divider orientation="vertical" />
      <RightPart
        onAttack={handleAttack}
        opponentCard={currentOpponentCard}
        userCards={currentUserCard}
      />
    </>
  );
}

const mapDispatchToProps = dispatch => {
  return {
    selectOpponentCard: card => dispatch(selectOpponentCard(card)),
    selectUserCard: card => dispatch(selectUserCard(card))
  };
};

const mapStateToProps = state => ({
  userName: state.user.user.login,
  opponentName: state.playground.opponent.login,
  userCards: state.playground.userCards,
  opponentCards: state.playground.opponentCards,
  currentUserCard: state.playground.currentUserCard,
  currentOpponentCard: state.playground.currentOpponentCard,
  palyground: state.playground,
  gameId : state.playground.gameId
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PlayGround);
