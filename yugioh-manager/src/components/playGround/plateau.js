import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Card from "../card/card";

const useStyles = makeStyles(theme => ({
  button: {
    margin: theme.spacing(1)
  }
}));



export default function Plateau({onClickCard, cards}) {
  const classes = useStyles();

  

  return (
    <Grid
      item
      xs
      container
      direction="row"
      justify="center"
      alignItems="center"
      spacing={3}
    >
      {cards.map((card, index) => (
        <Grid item xs={3}>
          <Card
            key={index}
            type="picture"
            card={card}
            size="small"
            onClickCard={() => onClickCard(card)}
          />
        </Grid>
      ))}
    </Grid>
  );
}
