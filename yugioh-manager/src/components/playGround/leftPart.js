import React from "react";
import { makeStyles } from "@material-ui/core/styles";

import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import EndTurnButton from "./endTurnButton";
import ActionPoint from "./actionPoint";

import User from "../user/user";

const useStyles = makeStyles({
  paper: {
    height: "100%",
    width: "100%",
    alignItems: "center"
  },
  container: {
    backgroundColor: "red"
  }
});

export default function SimpleCard({ userName, opponentName, onClickEndTurn}) {
  const classes = useStyles();

  return (
    <Grid
      item
      xs
      container
      direction="column"
      justify="space-between"
      alignItems="center"
    >
      <Grid item>
        <User name={userName} type="avatar" enabledText={true} />
      </Grid>
      <Grid item>
        <ActionPoint />
      </Grid>
      <Grid item>
        <EndTurnButton onClick={onClickEndTurn}/>
      </Grid>
      <Grid item>
        <ActionPoint />
      </Grid>
      <Grid item>
        <User name={opponentName} type="avatar" enabledText={true} />
      </Grid>
    </Grid>
  );
}
