import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Route, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import Header from "../components/header";
import Sidebar from "../components/sidebar";

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex"
  },
  toolbar: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    padding: theme.spacing(0, 1),
    ...theme.mixins.toolbar
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(1)
  }
}));

function AuthenticatedLayout({ component: MatchedPage, props, ...rest }) {
  const { isAuthenticated } = rest.auth;
  const [open, setOpen] = React.useState(false);
  const classes = useStyles();

  const handleDrawerOpen = () => {
    setOpen(!open);
  };

  return (
    <Route
      {...rest}
      render={props =>
        isAuthenticated ? (
          <div className={classes.root}>
            <Header
              label="Page name"
              money={5000}
              onToogleSidebar={handleDrawerOpen}
              open={open}
            />
            <Sidebar open={open} />
            <main className={classes.content}>
              <div className={classes.toolbar} />
              <MatchedPage {...props} />
            </main>
          </div>
        ) : (
          <Redirect
            to={{
              pathname: "/connexion",
              state: { from: props.location }
            }}
          />
        )
      }
    />
  );
}

const mapStateToProps = state => {
  return {
    auth: state.auth
  };
};

export default connect(mapStateToProps)(AuthenticatedLayout);
