import axios from "axios";
import Config from "../../../constants/backendConfig.json";

 export async function authentification(login, password) {
     const res = await axios.get(
       `${Config.SPRINGBOOT}/auth?login=${login}&pwd=${password}`
     );
     return await res.data;
     // need to check error
 }

 // add user to database
export async  function signUp(loginForm) {
    const res = await axios.post(
      `${Config.SPRINGBOOT}/user`, loginForm
    );
    return await res.data;
}

// get user by id
export async  function getUser(userId) {
  const res = await axios.get(
    `${Config.SPRINGBOOT}/user/${userId}`
  );
  return await res.data;
}

// update user
export async  function updateUser(userId, user) {
  const res = await axios.put(
    `${Config.SPRINGBOOT}/user/${userId}`, user
  );
  return await res.data;
}

