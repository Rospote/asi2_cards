import axios from "axios";
import Config from "../../../constants/backendConfig.json";

// get all card
export async function getAllCards() {
  const res = await axios.get(`${Config.SPRINGBOOT}/cards`);
  return await res.data;
}

// get all card in store
export async function getcardsInStore() {
  const res = await axios.get(`${Config.SPRINGBOOT}/cards_to_sell`);
  return await res.data;
}

// get card by user
export async function getCardsByUser(id) {
  var url = `${Config.SPRINGBOOT}/user/${id.toString()}`;
  const res = await axios.get(url);
  return await res.data.cardList;
}

export async function changeUserIdCard(idCard, card) {
  const res = await axios.put(`${Config.SPRINGBOOT}/card/${idCard.toString()}`, card);
  return await res.data;
}

export async function sellCardRequest(card, userId ) {
  const request = {cardId: card.id, userId:userId}
  const res = await axios.post(`${Config.SPRINGBOOT}/sell_card`, request);
  return await res.data;
}

export async function buyCardRequest(card, userId) {

  const request = {cardId: card.id, userId:userId}
  const res = await axios.post(`${Config.SPRINGBOOT}/buy_card`, request);
  return await res.data;
}
