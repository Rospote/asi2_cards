import { START_TYPING, STOP_TYPING } from "./action-types";

const initialState = { typerList: [] };

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case START_TYPING:
      console.log(action.username)
      return {
        ...state,
        typerList: [...state.typerList, action.username]
      };
    case STOP_TYPING:
      return {...state,
        typerList:state.typerList.filter(user => user.id !== action.username.id)}
    default:
      return state;
  }
};
