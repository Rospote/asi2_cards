import {
 SELECT_GAME_OPPONENT_CARD,
 SELECT_GAME_USER_CARD,
 LOAD_PLAYGROUND,
} from "./action-types";

export function selectOpponentCard(card) {
  console.log("selectOpponentCard" + card);
  return {
    type: SELECT_GAME_OPPONENT_CARD,
    card: card
  };
}

export function selectUserCard(card) {
  console.log("selectUserCard" + card);
  return {
    type: SELECT_GAME_USER_CARD,
    card: card
  };
}

export function loadPlayground(playground) {
  console.log("loadPlayground");
  console.log(playground);

  return {
    type: LOAD_PLAYGROUND,
    playground: playground
  };
}