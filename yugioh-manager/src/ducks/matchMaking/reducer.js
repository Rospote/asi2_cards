import {
  ADD_CARD_TO_MATCH_MAKING,
  REMOVE_CARD_TO_MACH_MAKING
} from "./action-types";

const initialState = {
  matchMakingCards: []
};

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case ADD_CARD_TO_MATCH_MAKING:
      return {
        ...state,
        matchMakingCards: [...state.matchMakingCards, action.card]
      };
    case REMOVE_CARD_TO_MACH_MAKING:
      const newStoreCardList = state.storeCardList.filter(
        item => item.id !== action.cardId
      );
      return { ...state, matchMakingCards: newStoreCardList };
    default:
      return state;
  }
};
