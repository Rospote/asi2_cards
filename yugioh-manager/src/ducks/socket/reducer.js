import { INIT_SOCKET_IO} from './action-types'

const initialState = {
    socket:""
};

export default (state = initialState, action ={}) => {
    switch (action.type) {
        case INIT_SOCKET_IO:
            return{
                socket: action.socket
            }
        default: return state;
    }
};
