import {
  ADD_CARD_TO_STORE,
  REMOVE_CARD_TO_STORE,
  LOAD_CARDS_TO_STORE,
  SELECT_CARD_STORE
} from "./action-types";

import { fakeStore } from "../../helper/fakeStore";

const initialState = {
  currentCard: null,
  storeCardList: fakeStore
};

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case ADD_CARD_TO_STORE:
      return { ...state, storeCardList: [...state.storeCardList, action.card] };
    case REMOVE_CARD_TO_STORE:
      const newStoreCardList = state.storeCardList.filter(
        item => item.id !== action.cardId
      );
      return { ...state, storeCardList: newStoreCardList };
    case LOAD_CARDS_TO_STORE:
      return { ...state, storeCardList: action.cards };
    case SELECT_CARD_STORE: {
      return { ...state, currentCard: action.card };
    }
    default:
      return state;
  }
};
