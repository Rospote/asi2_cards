export const ADD_CARD_TO_STORE = "ADD_CARD_TO_STORE";
export const REMOVE_CARD_TO_STORE = "REMOVE_CARD_TO_STORE";
export const LOAD_CARDS_TO_STORE = "LOAD_CARDS_TO_STORE";
export const SELECT_CARD_STORE = "SELECT_CARD_STORE";
