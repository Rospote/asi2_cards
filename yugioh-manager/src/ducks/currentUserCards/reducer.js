import { BUY_CARD, SELL_CARD, SELECT_CARD_USER, LOAD_CARD } from "./action-types";
import { fakeUserCards } from "../../helper/fakeUserCards";

const initialState = {
  currentCard: null,
  userCardList: fakeUserCards
};

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case BUY_CARD:
      return { ...state, userCardList: [...state.userCardList, action.card] };
    case SELL_CARD:
      const newUserCardList = state.userCardList.filter(
        item => item.id !== action.cardId
      );
      return { ...state, userCardList: newUserCardList };
    case SELECT_CARD_USER:
      return { ...state, currentCard: action.card };
    case LOAD_CARD: {
      return { ...state, userCardList: action.cards };
    }
    default:
      return state;
  }
};
