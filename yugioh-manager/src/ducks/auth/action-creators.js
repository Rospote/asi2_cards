
import { SET_CURRENT_USER, DELETE_CURRENT_USER,SET_CURRENT_USER_ROLE } from './action-types'

export function setCurrentUser(user) {
    return {
        type: SET_CURRENT_USER,
        user
    }
}

export function deleteCurrentUser() {
    localStorage.removeItem();
    return {
        type: DELETE_CURRENT_USER,
    }
}

export function setUserRole(role) {
    return {
        type: SET_CURRENT_USER_ROLE,
        role
    }
}




