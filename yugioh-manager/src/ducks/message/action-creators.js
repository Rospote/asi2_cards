import { ADD_USER, SEND_MESSAGE, SET_USER, SELECT_CHANNEL } from "./action-types";
import Config from "../../constants/backendConfig.json";

export function addUserToRoom(user) {
  console.log("add user to room");

  const message = {
    user: user,
    message: `user ${user} add to room`,
    isCurrent: true
  };
  return {
    type: ADD_USER,
    message
  };
}

export function selectChannel(channel) {
  console.log("add user to room");
  return {
    type: SELECT_CHANNEL,
    channel:channel
  };
}

export function sendMessageToRoom(m, isCurrent) {
  
  const message = {
    channelID:m.channelID,
    user: m.user,
    message: m.text,
  };

  return {
    type: SEND_MESSAGE,
    message
  };
}

export function setChatUser(user) {
 

  return {
    type: SET_USER,
    user
  };
}
