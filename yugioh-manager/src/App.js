import React from "react";
import { Provider } from "react-redux";
import "./App.css";

import Config from "./config/config.json";
import setAuthorizationToken from "./utils/authorizationHeader";

import Router from "./routes";

import CssBaseline from "@material-ui/core/CssBaseline";

import { createStore, combineReducers } from "redux";

import { setCurrentUser } from "./ducks/auth";
import { loadCardToStore } from "./ducks/currentStoreCards";
import { loadCard } from "./ducks/currentUserCards";
import { matchMakingReducer } from "./ducks/matchMaking";
import { authReducer } from "./ducks/auth";
import { chatReducer } from "./ducks/message";
import { cardUserReducer } from "./ducks/currentUserCards";
import { cardStoreReducer } from "./ducks/currentStoreCards";
import { userReducer, loadUser } from "./ducks/currentUser";
import { typersReducer } from "./ducks/typers";
import { gameReducer } from "./ducks/currentGameCards";
import { socketReducer, initSocketIo } from "./ducks/socket";

import { getcardsInStore, changeUserIdCard } from "./api/springBackend/card";
import { getUser } from "./api/springBackend/user";
import { getCardsByUser } from "./api/springBackend/card";

import {SocketProvider} from "./container/socketContext";


import Configuration from "./constants/backendConfig.json"

import io from "socket.io-client";
//import { setCurrentUser } from "./ducks/auth";


// create Store with combine reducer
const combineReducer = combineReducers({
  auth: authReducer,
  chat: chatReducer,
  cardUser: cardUserReducer,
  cardStore: cardStoreReducer,
  playground: gameReducer,
  user: userReducer,
  typers: typersReducer,
  matchMaking: matchMakingReducer,
  socket: socketReducer
});

const store = createStore(combineReducer);

//Set authorization token

if (localStorage[Config.ACCESS_TOKEN]) {
  setAuthorizationToken(localStorage[Config.ACCESS_TOKEN]);
  store.dispatch(setCurrentUser(localStorage[Config.ACCESS_TOKEN]));
  const userId = localStorage[Config.ACCESS_TOKEN].replace("RandomToken", "");
  console.log("applicationgln");
  console.log(userId);
  getUser(userId).then(response => {
    store.dispatch(loadCard(response.cardList));
    store.dispatch(loadUser(response));
  });
}

let socket = io(Configuration.NODE);

console.log(socket)

getcardsInStore().then(response => {
  store.dispatch(loadCardToStore(response));
});

function App() {
  return (
    <Provider store={store}>
      <SocketProvider value={socket}>
        <CssBaseline />
        
        <Router />
      </SocketProvider>
    </Provider>
  );
}

export default App;
