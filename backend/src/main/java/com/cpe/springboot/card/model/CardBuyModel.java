package com.cpe.springboot.card.model;

public class CardBuyModel {

    private Integer userId;
    private Integer cardId;

    public CardBuyModel() {
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getCardId() {
        return cardId;
    }

    public void setCardId(Integer cardId) {
        this.cardId = cardId;
    }
}
